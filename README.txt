1) Install the module in the normal way
2) Navigate to http://yoursite.com?q=admin/user/user_views
   - Enable the view counter (1st option)
	 - Configure the imagecache preset for the most viewed block and page if you wish to use them
	
3) Enable the "most viewed" block @ http://yoursite.com?q=admin/build/block (optional)
4) Configure access control to determine which roles can see views counts
That's it!